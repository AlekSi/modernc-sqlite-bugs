package moderncsqlitebugs

import (
	"database/sql"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	_ "modernc.org/sqlite" // register database/sql driver
)

func openDB(t *testing.T) {
	db, err := sql.Open("sqlite", ":memory:")
	if !assert.NoError(t, err) {
		return
	}

	defer db.Close()

	db.SetConnMaxIdleTime(0)
	db.SetConnMaxLifetime(0)
	db.SetMaxIdleConns(1)
	db.SetMaxOpenConns(1)

	if !assert.NoError(t, db.Ping()) {
		return
	}
}

func TestRace(t *testing.T) {
	const n = 1000
	var wg sync.WaitGroup
	ready := make(chan struct{}, n)
	start := make(chan struct{})

	wg.Add(n)
	for i := 0; i < n; i++ {
		go func() {
			defer wg.Done()

			ready <- struct{}{}
			<-start

			openDB(t)
		}()
	}

	for i := 0; i < n; i++ {
		<-ready
	}
	close(start)
	wg.Wait()
}
